## Classes

<dl>
<dt><a href="#StopWordFilter">StopWordFilter</a></dt>
<dd><p>StopWordFilter
A class for filtering out stop words from a given list of words.</p>
</dd>
<dt><a href="#TextFormatter">TextFormatter</a></dt>
<dd><p>TextFormatter
A class for text formatting operations.</p>
</dd>
</dl>

<a name="StopWordFilter"></a>

## StopWordFilter
StopWordFilter
A class for filtering out stop words from a given list of words.

**Kind**: global class  

* [StopWordFilter](#StopWordFilter)
    * [new StopWordFilter([stopWords])](#new_StopWordFilter_new)
    * _instance_
        * [.setStopWords(stopWords)](#StopWordFilter+setStopWords)
        * [.filter(words)](#StopWordFilter+filter) ⇒ <code>Array.&lt;string&gt;</code>
    * _static_
        * [.defaultStopWords](#StopWordFilter.defaultStopWords) ⇒ <code>Array.&lt;string&gt;</code>
        * [.extendedStopWords](#StopWordFilter.extendedStopWords) ⇒ <code>Array.&lt;string&gt;</code>

<a name="new_StopWordFilter_new"></a>

### new StopWordFilter([stopWords])

| Param | Type | Description |
| --- | --- | --- |
| [stopWords] | <code>Array.&lt;string&gt;</code> | An array of stop words. |

<a name="StopWordFilter+setStopWords"></a>

### stopWordFilter.setStopWords(stopWords)
Sets the stop words.

**Kind**: instance method of [<code>StopWordFilter</code>](#StopWordFilter)  

| Param | Type | Description |
| --- | --- | --- |
| stopWords | <code>Array.&lt;string&gt;</code> | An array of stop words. |

<a name="StopWordFilter+filter"></a>

### stopWordFilter.filter(words) ⇒ <code>Array.&lt;string&gt;</code>
Filters out stop words from the given list of words.

**Kind**: instance method of [<code>StopWordFilter</code>](#StopWordFilter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of words without stop words.  

| Param | Type | Description |
| --- | --- | --- |
| words | <code>Array.&lt;string&gt;</code> | An array of words. |

<a name="StopWordFilter.defaultStopWords"></a>

### StopWordFilter.defaultStopWords ⇒ <code>Array.&lt;string&gt;</code>
**Kind**: static property of [<code>StopWordFilter</code>](#StopWordFilter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of default stop words.  
<a name="StopWordFilter.extendedStopWords"></a>

### StopWordFilter.extendedStopWords ⇒ <code>Array.&lt;string&gt;</code>
**Kind**: static property of [<code>StopWordFilter</code>](#StopWordFilter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of extended stop words.  
<a name="TextFormatter"></a>

## TextFormatter
TextFormatter
A class for text formatting operations.

**Kind**: global class  

* [TextFormatter](#TextFormatter)
    * [new TextFormatter([stopWords])](#new_TextFormatter_new)
    * _instance_
        * [.setStopWords(stopWords)](#TextFormatter+setStopWords)
        * [.toString(text)](#TextFormatter+toString) ⇒ <code>string</code>
        * [.normalizeSearchQuery(text)](#TextFormatter+normalizeSearchQuery) ⇒ <code>string</code>
        * [.stripPunctuation(text)](#TextFormatter+stripPunctuation) ⇒ <code>string</code>
        * [.toUpperCase(text)](#TextFormatter+toUpperCase) ⇒ <code>string</code>
        * [.toLowerCase(text)](#TextFormatter+toLowerCase) ⇒ <code>string</code>
        * [.splitWords(text)](#TextFormatter+splitWords) ⇒ <code>Array.&lt;string&gt;</code>
        * [.filterStopWords(words)](#TextFormatter+filterStopWords) ⇒ <code>Array.&lt;string&gt;</code>
        * [.normalize(text, [upper])](#TextFormatter+normalize) ⇒ <code>string</code>
        * [.tokenize(text, [filterStopWords])](#TextFormatter+tokenize) ⇒ <code>Array.&lt;string&gt;</code>
    * _static_
        * [.TextFormatter](#TextFormatter.TextFormatter) ⇒ [<code>TextFormatter</code>](#TextFormatter)
        * [.StopWordFilter](#TextFormatter.StopWordFilter) ⇒ [<code>StopWordFilter</code>](#StopWordFilter)

<a name="new_TextFormatter_new"></a>

### new TextFormatter([stopWords])

| Param | Type | Description |
| --- | --- | --- |
| [stopWords] | <code>Array.&lt;string&gt;</code> | An array of stop words. |

<a name="TextFormatter+setStopWords"></a>

### textFormatter.setStopWords(stopWords)
Sets the stop words for the underlying StopWordFilter.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  

| Param | Type | Description |
| --- | --- | --- |
| stopWords | <code>Array.&lt;string&gt;</code> | An array of stop words. |

<a name="TextFormatter+toString"></a>

### textFormatter.toString(text) ⇒ <code>string</code>
Converts input to a string.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The input as a string.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>any</code> | The input to be converted. |

<a name="TextFormatter+normalizeSearchQuery"></a>

### textFormatter.normalizeSearchQuery(text) ⇒ <code>string</code>
Normalizes a search query.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The normalized text.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to be normalized. |

<a name="TextFormatter+stripPunctuation"></a>

### textFormatter.stripPunctuation(text) ⇒ <code>string</code>
Strips punctuation from the text.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The text without punctuation.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to be processed. |

<a name="TextFormatter+toUpperCase"></a>

### textFormatter.toUpperCase(text) ⇒ <code>string</code>
Converts the text to uppercase.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The text in uppercase.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to be converted. |

<a name="TextFormatter+toLowerCase"></a>

### textFormatter.toLowerCase(text) ⇒ <code>string</code>
Converts the text to lowercase.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The text in lowercase.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to be converted. |

<a name="TextFormatter+splitWords"></a>

### textFormatter.splitWords(text) ⇒ <code>Array.&lt;string&gt;</code>
Splits the text into words.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of words.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to be split. |

<a name="TextFormatter+filterStopWords"></a>

### textFormatter.filterStopWords(words) ⇒ <code>Array.&lt;string&gt;</code>
Filters out stop words from the list of words.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of words without stop words.  

| Param | Type | Description |
| --- | --- | --- |
| words | <code>Array.&lt;string&gt;</code> | An array of words. |

<a name="TextFormatter+normalize"></a>

### textFormatter.normalize(text, [upper]) ⇒ <code>string</code>
Normalizes the text and optionally converts to uppercase.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>string</code> - The normalized text.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| text | <code>string</code> |  | The text to be normalized. |
| [upper] | <code>boolean</code> | <code>false</code> | Whether to convert the text to uppercase. |

<a name="TextFormatter+tokenize"></a>

### textFormatter.tokenize(text, [filterStopWords]) ⇒ <code>Array.&lt;string&gt;</code>
Tokenizes the text into individual words.

**Kind**: instance method of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of tokens.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| text | <code>string</code> |  | The text to tokenize. |
| [filterStopWords] | <code>boolean</code> | <code>true</code> | Whether to filter out stop words. |

<a name="TextFormatter.TextFormatter"></a>

### TextFormatter.TextFormatter ⇒ [<code>TextFormatter</code>](#TextFormatter)
**Kind**: static property of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: [<code>TextFormatter</code>](#TextFormatter) - The TextFormatter class.  
<a name="TextFormatter.StopWordFilter"></a>

### TextFormatter.StopWordFilter ⇒ [<code>StopWordFilter</code>](#StopWordFilter)
**Kind**: static property of [<code>TextFormatter</code>](#TextFormatter)  
**Returns**: [<code>StopWordFilter</code>](#StopWordFilter) - The StopWordFilter class.  
