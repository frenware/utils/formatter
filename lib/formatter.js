const { _, log } = require('basd')

/**
 * @class StopWordFilter
 * A class for filtering out stop words from a given list of words.
 */
class StopWordFilter {
  /**
   * @static
   * @returns {Array<string>} An array of default stop words.
   */
  static get defaultStopWords() {
    return ['the', 'and', 'of', 'in', 'to', 'for']
  }

  /**
   * @static
   * @returns {Array<string>} An array of extended stop words.
   */
  static get extendedStopWords() {
    return [
      'a', 'about', 'above', 'after', 'again', 'against', 'ain', 'all', 'am', 'an',
      'and', 'any', 'are', 'aren', "aren't", 'as', 'at', 'be', 'because', 'been',
      'before', 'being', 'below', 'between', 'both', 'but', 'by', 'can', 'couldn',
      "couldn't", 'd', 'did', 'didn', "didn't", 'do', 'does', 'doesn', "doesn't",
      'doing', 'don', "don't", 'down', 'during', 'each', 'few', 'for', 'from', 'further',
      'had', 'hadn', "hadn't", 'has', 'hasn', "hasn't", 'have', 'haven', "haven't", 'having',
      'he', 'her', 'here', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'i', 'if', 'in',
      'into', 'is', 'isn', "isn't", 'it', "it's", 'its', 'itself', 'just', 'll', 'm', 'ma',
      'me', 'mightn', "mightn't", 'more', 'most', 'mustn', "mustn't", 'my', 'myself',
      'needn', "needn't", 'no', 'nor', 'not', 'now', 'o', 'of', 'off', 'on', 'once', 'only',
      'or', 'other', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 're', 's', 'same',
      'shan', "shan't", 'she', "she's", 'should', "should've", 'shouldn', "shouldn't", 'so',
      'some', 'such', 't', 'than', 'that', "that'll", 'the', 'their', 'theirs', 'them',
      'themselves', 'then', 'there', 'these', 'they', 'this', 'those', 'through', 'to', 'too',
      'under', 'until', 'up', 've', 'very', 'was', 'wasn', "wasn't", 'we', 'were', 'weren',
      "weren't", 'what', 'when', 'where', 'which', 'while', 'who', 'whom', 'why', 'will',
      'with', 'won', "won't", 'wouldn', "wouldn't", 'y', 'you', "you'd", "you'll", "you're",
      "you've", 'your', 'yours', 'yourself', 'yourselves'
    ]
  }

  /**
   * @constructor
   * @param {Array<string>} [stopWords] An array of stop words.
   */
  constructor(stopWords) {
    this.stopWords = new Set(stopWords || this.constructor.defaultStopWords)
  }

  /**
   * Sets the stop words.
   * @param {Array<string>} stopWords An array of stop words.
   */
  setStopWords(stopWords) {
    this.stopWords = new Set(stopWords)
  }

  /**
   * Filters out stop words from the given list of words.
   * @param {Array<string>} words An array of words.
   * @returns {Array<string>} An array of words without stop words.
   */
  filter(words) {
    return words.filter(word => !this.stopWords.has(word))
  }
}

/**
 * @class TextFormatter
 * A class for text formatting operations.
 */
class TextFormatter {
  /**
   * @static
   * @returns {TextFormatter} The TextFormatter class.
   */
  static get TextFormatter() { return TextFormatter }

  /**
   * @static
   * @returns {StopWordFilter} The StopWordFilter class.
   */
  static get StopWordFilter() { return StopWordFilter }

  /**
   * @constructor
   * @param {Array<string>} [stopWords] An array of stop words.
   */
  constructor(stopWords) {
    this.stopWordFilter = new StopWordFilter(stopWords)
  }

  /**
   * Sets the stop words for the underlying StopWordFilter.
   * @param {Array<string>} stopWords An array of stop words.
   */
  setStopWords(stopWords) {
    this.stopWordFilter.setStopWords(stopWords)
  }

  /**
   * Converts input to a string.
   * @param {any} text The input to be converted.
   * @returns {string} The input as a string.
   */
  toString(text) {
    return String(text)
  }

  /**
   * Normalizes a search query.
   * @param {string} text The text to be normalized.
   * @returns {string} The normalized text.
   */
  normalizeSearchQuery(text) {
    return this.toString(text)
      .toUpperCase()
      .replace(/[^\w\s&|()"]/g, '') // Remove punctuation except ", &, |, (, and )
      .replace(/&/g, ' AND ') // Replace "&" with " AND "
      .replace(/\|/g, ' OR ') // Replace "|" with " OR "
      .replace(/\s+/g, ' ') // Remove multiple spaces
      .trim() // Remove leading and trailing spaces
  }

  /**
   * Strips punctuation from the text.
   * @param {string} text The text to be processed.
   * @returns {string} The text without punctuation.
   */
  stripPunctuation(text) {
    return this.toString(text)
      .replace(/[^\w\s]|_/g, '')
      .replace(/\s+/g, ' ')
      .trim()
  }

  /**
   * Converts the text to uppercase.
   * @param {string} text The text to be converted.
   * @returns {string} The text in uppercase.
   */
  toUpperCase(text) {
    return this.toString(text).toUpperCase()
  }

  /**
   * Converts the text to lowercase.
   * @param {string} text The text to be converted.
   * @returns {string} The text in lowercase.
   */
  toLowerCase(text) {
    return this.toString(text).toLowerCase()
  }

  /**
   * Splits the text into words.
   * @param {string} text The text to be split.
   * @returns {Array<string>} An array of words.
   */
  splitWords(text) {
    return this.toString(text).split(' ')
  }

  /**
   * Filters out stop words from the list of words.
   * @param {Array<string>} words An array of words.
   * @returns {Array<string>} An array of words without stop words.
   */
  filterStopWords(words) {
    return this.stopWordFilter.filter(words)
  }

  /**
   * Normalizes the text and optionally converts to uppercase.
   * @param {string} text The text to be normalized.
   * @param {boolean} [upper=false] Whether to convert the text to uppercase.
   * @returns {string} The normalized text.
   */
  normalize(text, upper = false) {
    const noPunctuation = this.stripPunctuation(text)
    if (upper)
      return this.toUpperCase(noPunctuation)
    return this.toLowerCase(noPunctuation)
  }

  /**
   * Tokenizes the text into individual words.
   * @param {string} text The text to tokenize.
   * @param {boolean} [filterStopWords=true] Whether to filter out stop words.
   * @returns {Array<string>} An array of tokens.
   */
  tokenize(text, filterStopWords = true) {
    const normalized = this.normalize(text)
    let tokens = this.splitWords(normalized)
    if (filterStopWords)
      tokens = this.filterStopWords(tokens)
    return tokens
  }
}

module.exports = TextFormatter
