# Formatter

[![npm](https://img.shields.io/npm/v/@basd/formatter?style=flat&logo=npm)](https://www.npmjs.com/package/@basd/formatter)
[![pipeline](https://gitlab.com/frenware/utils/formatter/badges/master/pipeline.svg)](https://gitlab.com/frenware/utils/formatter/-/pipelines)
[![license](https://img.shields.io/npm/l/@basd/formatter)](https://gitlab.com/frenware/utils/formatter/-/blob/master/LICENSE)
[![downloads](https://img.shields.io/npm/dw/@basd/formatter)](https://www.npmjs.com/package/@basd/formatter) 

[![Gitlab](https://img.shields.io/badge/Gitlab%20-%20?logo=gitlab&color=%23383a40)](https://gitlab.com/frenware/utils/formatter)
[![Twitter](https://img.shields.io/badge/@basdwon%20-%20?logo=twitter&color=%23383a40)](https://twitter.com/basdwon)
[![Discord](https://img.shields.io/badge/Basedwon%20-%20?logo=discord&color=%23383a40)](https://discordapp.com/users/basedwon)

A comprehensive text formatting and manipulation library written in JS. It aims to offer a simple and efficient way to format, normalize, tokenize, and filter text in various scenarios.

## Installation

Install the package with:

```bash
npm install @basd/formatter
```

## Usage

First, import the `Formatter` library.

```js
import Formatter from '@basd/formatter'
```
or
```js
const Formatter = require('@basd/formatter')
```

Or import `TextFormatter` and `StopWordFilter` classes like this:

```js
const { TextFormatter, StopWordFilter } = require('@basd/formatter')
```

### TextFormatter

```js
const formatter = new TextFormatter()

console.log(formatter.normalizeSearchQuery('hello & world | earth'))
// Output: 'HELLO AND WORLD OR EARTH'

console.log(formatter.toUpperCase('hello world'))
// Output: 'HELLO WORLD'

console.log(formatter.toLowerCase('HELLO WORLD'))
// Output: 'hello world'

console.log(formatter.stripPunctuation('hello, world!'))
// Output: 'hello world'

const tokens = formatter.tokenize('This is an example.')
console.log(tokens)
// Output: ['this', 'is', 'an', 'example']
```

### StopWordFilter

```js
const stopWordFilter = new StopWordFilter(['a', 'an', 'the'])
console.log(stopWordFilter.filter(['this', 'is', 'a', 'test']))
// Output: ['this', 'is', 'test']
```

## Documentation

- [API Reference](/docs/api.md)

### TextFormatter

#### Methods

- `normalizeSearchQuery(text: string): string`
- `stripPunctuation(text: string): string`
- `toUpperCase(text: string): string`
- `toLowerCase(text: string): string`
- `splitWords(text: string): Array<string>`
- `filterStopWords(words: Array<string>): Array<string>`
- `normalize(text: string, upper: boolean = false): string`
- `tokenize(text: string, filterStopWords: boolean = true): Array<string>`
- `setStopWords(stopWords: Array<string>)`: Set a new list of stop words.

### StopWordFilter

#### Methods

- `filter(words: Array<string>): Array<string>`
- `setStopWords(stopWords: Array<string>)`: Set a new list of stop words.

#### Static Methods

- `get defaultStopWords(): Array<string>`: Get the default list of stop words.
- `get extendedStopWords(): Array<string>`: Get an extended list of stop words. (`extendedStopWords` must be imported or defined.)

## Tests

In order to run the test suite, simply clone the repository and install its dependencies:

```bash
git clone https://gitlab.com/frenware/utils/formatter.git
cd formatter
npm install
```

To run the tests:

```bash
npm test
```

## Contributing

Thank you! Please see our [contributing guidelines](/docs/contributing.md) for details.

## Donations

If you find this project useful and want to help support further development, please send us some coin. We greatly appreciate any and all contributions. Thank you!

**Bitcoin (BTC):**
```
1JUb1yNFH6wjGekRUW6Dfgyg4J4h6wKKdF
```

**Monero (XMR):**
```
46uV2fMZT3EWkBrGUgszJCcbqFqEvqrB4bZBJwsbx7yA8e2WBakXzJSUK8aqT4GoqERzbg4oKT2SiPeCgjzVH6VpSQ5y7KQ
```

## License

@basd/formatter is [MIT licensed](https://gitlab.com/frenware/utils/formatter/-/blob/master/LICENSE).
