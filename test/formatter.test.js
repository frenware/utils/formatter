const { StopWordFilter, TextFormatter } = require('../lib/formatter')

describe('StopWordFilter', () => {
  it('should use default stop words if none are provided', () => {
    const filter = new StopWordFilter()
    const words = ['the', 'quick', 'brown', 'fox']
    expect(filter.filter(words)).to.eql(['quick', 'brown', 'fox'])
  })

  it('should use provided stop words', () => {
    const filter = new StopWordFilter(['quick', 'fox'])
    const words = ['the', 'quick', 'brown', 'fox']
    expect(filter.filter(words)).to.eql(['the', 'brown'])
  })

  it('should update stop words', () => {
    const filter = new StopWordFilter(['quick', 'fox'])
    filter.setStopWords(['brown'])
    const words = ['the', 'quick', 'brown', 'fox']
    expect(filter.filter(words)).to.eql(['the', 'quick', 'fox'])
  })
})

describe('TextFormatter', () => {
  let formatter
  beforeEach(() => {
    formatter = new TextFormatter()
  })

  it('should normalize a search query', () => {
    const text = 'test & cases|are awesome'
    expect(formatter.normalizeSearchQuery(text)).to.equal('TEST AND CASES OR ARE AWESOME')
  })

  it('should strip punctuation', () => {
    const text = 'Hello, world!'
    expect(formatter.stripPunctuation(text)).to.equal('Hello world')
  })

  it('should convert text to uppercase', () => {
    const text = 'Hello'
    expect(formatter.toUpperCase(text)).to.equal('HELLO')
  })

  it('should convert text to lowercase', () => {
    const text = 'Hello'
    expect(formatter.toLowerCase(text)).to.equal('hello')
  })

  it('should split text into words', () => {
    const text = 'Hello world'
    expect(formatter.splitWords(text)).to.eql(['Hello', 'world'])
  })

  it('should filter stop words', () => {
    const words = ['the', 'quick', 'brown', 'fox']
    expect(formatter.filterStopWords(words)).to.eql(['quick', 'brown', 'fox'])
  })

  it('should normalize text', () => {
    const text = 'HeLLo, WOrLD!'
    expect(formatter.normalize(text, true)).to.equal('HELLO WORLD')
    expect(formatter.normalize(text, false)).to.equal('hello world')
  })

  it('should tokenize text', () => {
    const text = 'the quick brown fox'
    expect(formatter.tokenize(text, true)).to.eql(['quick', 'brown', 'fox'])
    expect(formatter.tokenize(text, false)).to.eql(['the', 'quick', 'brown', 'fox'])
  })
})
