declare module '@basd/formatter' {
  class StopWordFilter {
    static readonly defaultStopWords: string[]
    static readonly extendedStopWords: string[]
    constructor(stopWords?: string[])
    setStopWords(stopWords: string[]): void
    filter(words: string[]): string[]
  }

  class TextFormatter {
    static readonly TextFormatter: typeof TextFormatter
    static readonly StopWordFilter: typeof StopWordFilter
    constructor(stopWords?: string[])
    setStopWords(stopWords: string[]): void
    toString(text: unknown): string
    normalizeSearchQuery(text: unknown): string
    stripPunctuation(text: unknown): string
    toUpperCase(text: unknown): string
    toLowerCase(text: unknown): string
    splitWords(text: unknown): string[]
    filterStopWords(words: string[]): string[]
    normalize(text: unknown, upper?: boolean): string
    tokenize(text: unknown, filterStopWords?: boolean): string[]
  }

  export { StopWordFilter, TextFormatter }
}
